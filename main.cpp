#include <iostream>
#include <sstream>
#include <unordered_set>
#include <vector>

using namespace std;

struct Position {
    int x;
    int y;
};

bool operator==(const Position& p1, const Position& p2) {
    return (p1.x == p2.x) && (p1.y == p2.y);
}

// little hack, acceptable here
struct pos_hash {
    int operator()(const Position& p) const {
        return 1000 * p.x + p.y;
    }
};

struct Ship {
    private:
        unordered_set<Position, pos_hash> body;
        unordered_set<Position, pos_hash> hits;
    public:
        Ship(const Position&, const Position&);
        bool is_sunken() { return this->body == this->hits; };
        bool is_hit() { return this->hits.size() != 0; };
        void shoot(const Position&);
};

Ship::Ship(const Position& p1, const Position& p2) {
    for (int x = p1.x; x <= p2.x; ++x)
        for (int y = p1.y; y <= p2.y; ++y)
            this->body.insert({x, y});
}

void Ship::shoot(const Position& pos) {
    for (Position p : this->body)
        if (p == pos)
            this->hits.insert(pos);
}

Position parse_pos(string& pos_str) {
    char letter = pos_str[0];
    int x = letter - 'A' + 1;

    string rest = pos_str.erase(0, 1);
    int y = stoi(rest);
    return {x, y};
}

vector<string> split(const string& input, char delim) {
    vector<string> out;

    stringstream ss(input);
    string chunk;

    while (std::getline(ss, chunk, delim))
        out.push_back(chunk);

    return out;
}

Ship parse_ship(const string& ship_pos) {
    stringstream ss(ship_pos);
    string pos;

    std::getline(ss, pos, ' ');
    Position p1 = parse_pos(pos);

    std::getline(ss, pos, ' ');
    Position p2 = parse_pos(pos);

    return Ship(p1, p2);
}

vector<Ship> parse_ships(string& ships_pos) {
    vector<Ship> out = {};

    vector<string> positions = split(ships_pos, ',');
    for (string pos : positions)
        out.push_back(parse_ship(pos));

    return out;
};

vector<Position> parse_hits(string& hits_pos) {
    vector<Position> out = {};

    vector<string> positions = split(hits_pos, ' ');
    for (string pos : positions)
        out.push_back(parse_pos(pos));

    return out;
}

string solution(int N, string& S, string& T) {
    vector<Ship> ships = parse_ships(S);
    vector<Position> hits = parse_hits(T);

    for (Ship &ship : ships)
        for (Position hit : hits)
            ship.shoot(hit);

    int sunken = 0;
    for (Ship ship : ships)
        if (ship.is_sunken())
            ++sunken;

    int just_hit = 0;
    for (Ship ship : ships)
        if (ship.is_hit() && !ship.is_sunken())
            ++just_hit;

    return to_string(sunken) + "," + to_string(just_hit);
}

int main() {
    string s = "A1 A3,B2 B2,C3 D4";
    string t = "A1 A2 B2 B3 C3 D3 C4 D4";
    cout << solution(4, s, t) << '\n';
    return 0;
}
